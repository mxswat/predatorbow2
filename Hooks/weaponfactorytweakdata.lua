
Hooks:PostHook( WeaponFactoryTweakData, "init", "PreadatorBowModInit", function(self)

---POISON ARROW NO AMMO PIC
self.parts.wpn_fps_bow_long_m_poison.custom_stats = {
			ammo_pickup_min_mul = 0, 
			ammo_pickup_max_mul = 0,
			can_shoot_through_shield = true,
			launcher_grenade = "long_poison_arrow",
			dot_data = {
				type = "poison",
				custom_data = {dot_length = 999, hurt_animation_chance = 0.05, dot_damage = 1,dot_trigger_max_distance = 3000,dot_tick_period = 1}
			}
		}	
end )
