Hooks:PostHook( WeaponTweakData, "init", "predator_bowModInit", function(self)
    self.predator_bow.attachment_points = {
        {
            name = "a_fl",
            base_a_obj = "a_fl",
            position = Vector3(4.5, 3, 8.4), -- visuale da dietro  dx/sx , Dietro/avanti , su giu
            rotation = Rotation(0, 0, -90)
        }        
    }
   	self.predator_bow.not_allowed_in_bleedout = false
	self.predator_bow.armor_piercing_chance = 1
	self.predator_bow.timers.unequip = 0.55
    self.predator_bow.timers.equip = 0.55
    self.predator_bow.bow_reload_speed_multiplier = 3
	self.predator_bow.charge_data = {}
    self.predator_bow.charge_data.max_t = 0.7 -- 30%% faster than plains
    self.predator_bow.CLIP_AMMO_MAX = 1
	self.predator_bow.NR_CLIPS_MAX = 35
	self.predator_bow.AMMO_MAX = self.predator_bow.CLIP_AMMO_MAX * self.predator_bow.NR_CLIPS_MAX
end )